<?php
namespace Modules\Order\Http\Controllers;

use Modules\Order\Http\Requests\GviajeRequest;
use Modules\Order\Http\Resources\GviajeCollection;
use Modules\Order\Http\Resources\GviajeResource;
use App\Models\Tenant\Catalogs\IdentityDocumentType;
use App\Http\Controllers\Controller;
use Modules\Order\Models\Gviaje;
use Illuminate\Http\Request;

class GviajeController extends Controller
{

    public function index()
    {
        return view('order::gviajes.index');
    }

    public function columns()
    {
        return [
            'descripcion' => 'Descripcion',
            'tipo' => 'Tipo',
        ];
    }

    public function records(Request $request)
    {

        $records = Gviaje::where($request->column, 'like', "%{$request->value}%")
            // ->where('order_id','=','1')
                            ->orderBy('descripcion');

        return new GviajeCollection($records->paginate(config('tenant.items_per_page')));
    }


    public function tables()
    {
        $identity_document_types = IdentityDocumentType::whereActive()->get();
        $api_service_token = config('configuration.api_service_token');

        return compact('identity_document_types', 'api_service_token');
    }

    public function record($id)
    {
        $record = new GviajeResource(Vehiculo::findOrFail($id));

        return $record;
    }

    public function store(GviajeRequest $request)
    {

        $id = $request->input('id');
        $record = Gviaje::firstOrNew(['id' => $id]);
        $record->fill($request->all());
        $record->save();

        return [
            'success' => true,
            'message' => ($id)?'Gastos Viaje editado con éxito':'Gastos registrado con éxito',
            'id' => $record->id
        ];
    }

    public function destroy($id)
    {

        $record = Gviaje::findOrFail($id);
        $record->delete();

        return [
            'success' => true,
            'message' => 'Vehiculo eliminado con éxito'
        ];

    }

}
