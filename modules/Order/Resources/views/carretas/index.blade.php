@extends('tenant.layouts.app')

@section('content')

    <tenant-carretas-index :type-user="{{json_encode(Auth::user()->type)}}"></tenant-carretas-index>

@endsection
