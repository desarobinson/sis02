@php
    $establishment = $document->establishment;
    $supplier = $document->supplier;
    
    $tittle = $document->series.'-'.str_pad($document->number, 8, '0', STR_PAD_LEFT); 
@endphp
<html>
<head>
    {{--<title>{{ $tittle }}</title>--}}
    {{--<link href="{{ $path_style }}" rel="stylesheet" />--}}
</head>
<body>
<table class="full-width">
    <tr>
       
        <td width="50%" class="pl-3">
            <div class="text-left">
                <h4 class="">{{ $company->name }}</h4>
                <h5>{{ 'RUC '.$company->number }}</h5>
                
            </div>
        </td>
        <td width="20%" class="border-box py-4 px-2 text-center">
            <h5 class="text-center">{{ $document->document_type->description}}</h5>
            <h3 class="text-center">{{ $tittle }}</h3>
        </td>
    </tr>
</table>
<table class="full-width mt-5">

<tr>
        <td width="15%">Cliente: {{ $document->customer}}</td>
        
      
    </tr>
    
    <tr>
       
        
        <td width="50%">Fecha de emisión:</td>
        <td width="40%">{{ $document->date_of_issue->format('Y-m-d') }}</td>
    </tr>
    
 
   
    <tr>
        <td class="align-top">Usuario:</td>
        <td colspan="3">
            {{ $document->user->name }}
        </td>
    </tr>
    @if($document->purchase_order)
    <tr>
        <td class="align-top">O. Compra:</td>
        <td  colspan="3">{{ $document->purchase_order->number_full }}</td>
    </tr>
    @endif
</table>
 

<table class="full-width mt-10 mb-10">
    <thead class="">
    <tr class="bg-grey">
       
        <th class="border-top-bottom text-left py-2">DESCRIPCIÓN</th>
       
        <th class="border-top-bottom text-right py-2" width="60%">TOTAL</th>
    </tr>
    </thead>
    <tbody>
    @foreach($document->items as $row)
        <tr>
            
            
            <td class="text-right align-top">{{$row->description }}</td>
            
            <td class="text-right align-top">{{ number_format($row->total, 2) }}</td>
        </tr>
        
    @endforeach
       
    </tbody>
</table>


<table class="full-width" style="margin-top:40px">
    <tr>
         
            <td colspan="6" class="border-bottom"></td>
        
    </tr>
    <tr>
         
            <td colspan="6" style="margin-left:20px"><b> FIRMA</b></td>
        
    </tr>
</table>
</body>
</html>
