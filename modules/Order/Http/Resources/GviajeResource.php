<?php

namespace Modules\Order\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GviajeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'order_id' => $this->order_id,
            'descripcion' => $this->descripcion,
            'tipo' => $this->tipo,
            'monto' => $this->monto,
            'galones' => $this->galones,
            'precio' => $this->precio,
            'fecha' => $this->fecha,
            'filename' => $this->filename,
            'documents_id' => $this->documents_id,            

        ];
    }
}
