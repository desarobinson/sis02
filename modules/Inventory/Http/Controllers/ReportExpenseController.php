<?php

namespace Modules\Inventory\Http\Controllers;

use App\Http\Controllers\Controller;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Tenant\Establishment;
use App\Models\Tenant\Company;
use App\Models\Tenant\Item;
use Modules\Inventory\Models\ItemWarehouse;
use Modules\Inventory\Exports\ExpenseExport;
use Modules\Inventory\Models\Warehouse;
use Modules\Expense\Models\Expense;
use Modules\Order\Models\Vehiculo;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class ReportExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        if($request->ruc=='20604353433'){
            
            $basedatos='tenancy_jl';
        }
        else {
            $basedatos='tenancy_hector';
        }
        
        if($request->tipo=='2')
        {
               
            if($request->warehouse_id && $request->warehouse_id != 'all')
            {
                $reports = DB::table($basedatos.'.expenses')
                ->select('expenses.id','expenses.placa','expenses.date_of_issue','expenses.total',DB::raw( ' (select name  from '.$basedatos.'.persons where  id='.$basedatos.'.expenses.supplier_id ) as proveedor'),DB::raw( ' (select description  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id  limit 1 ) as descripcionitem'),DB::raw( ' (select sum(galones)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as galones'),DB::raw( ' (select sum(precio)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as precio'),DB::raw( ' (select description from '.$basedatos.'.expense_reasons where  id='.$basedatos.'.expenses.expense_reason_id ) as motivo'))
                ->where(''.$basedatos.'.expenses.placa','=',''.$request->warehouse_id.'')
                ->where(''.$basedatos.'.expenses.state_type_id','!=','11')
                ->whereIn(''.$basedatos.'.expenses.expense_reason_id',['6','7'])
                ->whereBetween(''.$basedatos.'.expenses.date_of_issue',[$request->dataFabricatiei,$request->dataFabricatiei2])
                 ->orderBy(''.$basedatos.'.expenses.created_at')->paginate(config('tenant.items_per_page'));
              
                // state_type_id !=11
                // $reports=DB::table($basedatos.'.order_notes')
                // ->select('order_notes.id','order_notes.placa','order_notes.servicio','order_notes.date_of_issue','order_notes.conductorname','order_notes.placa','order_notes.total',DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=4 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as gv'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=5 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as go'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=6 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as hyo'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=7 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as lima'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=8 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as estibaje'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=8 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as estibaje'),DB::raw( ' (select name  from '.$basedatos.'.persons where id='.$basedatos.'.order_notes.customer_id ) as cliente'))
                // ->join(''.$basedatos.'.expenses',''.$basedatos.'.expenses.order_id','=',''.$basedatos.'.order_notes.id')
                // ->where(''.$basedatos.'.order_notes.placa','=',$request->warehouse_id)
                // ->groupBy('order_notes.id')
                // ->orderBy(''.$basedatos.'.order_notes.created_at')->paginate(config('tenant.items_per_page'));
    
            }
            else{
                $reports = DB::table($basedatos.'.expenses')
                ->select('expenses.id','expenses.placa','expenses.date_of_issue','expenses.total',DB::raw( ' (select name  from '.$basedatos.'.persons where  id='.$basedatos.'.expenses.supplier_id ) as proveedor'),DB::raw( ' (select description  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id  limit 1 ) as descripcionitem'),DB::raw( ' (select sum(galones)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as galones'),DB::raw( ' (select sum(precio)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as precio'),DB::raw( ' (select description from '.$basedatos.'.expense_reasons where  id='.$basedatos.'.expenses.expense_reason_id ) as motivo'))
                ->where(''.$basedatos.'.expenses.state_type_id','!=','11')
                ->whereIn(''.$basedatos.'.expenses.expense_reason_id',['6','7'])
                ->whereBetween(''.$basedatos.'.expenses.date_of_issue',[$request->dataFabricatiei,$request->dataFabricatiei2])
                 ->orderBy(''.$basedatos.'.expenses.created_at')->paginate(config('tenant.items_per_page'));
            }
        }
        else if ( $request->tipo=='3')
             {

                    if($request->warehouse_id && $request->warehouse_id != 'all')
                    {
                        $reports = DB::table($basedatos.'.expenses')
                        ->select('expenses.id','expenses.placa','expenses.date_of_issue','expenses.total',DB::raw( ' (select name  from '.$basedatos.'.persons where  id='.$basedatos.'.expenses.supplier_id ) as proveedor'),DB::raw( ' (select description  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id  limit 1 ) as descripcionitem'),DB::raw( ' (select sum(galones)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as galones'),DB::raw( ' (select sum(precio)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as precio'),DB::raw( ' (select description from '.$basedatos.'.expense_reasons where  id='.$basedatos.'.expenses.expense_reason_id ) as motivo'))
                        ->where(''.$basedatos.'.expenses.placa','=',''.$request->warehouse_id.'')
                        ->where(''.$basedatos.'.expenses.state_type_id','!=','11')
                        ->whereIn(''.$basedatos.'.expenses.expense_reason_id',['1','2'.'3','4','5','8'])
                        ->whereBetween(''.$basedatos.'.expenses.date_of_issue',[$request->dataFabricatiei,$request->dataFabricatiei2])
                        ->orderBy(''.$basedatos.'.expenses.created_at')->paginate(config('tenant.items_per_page'));
                    
            
                        // $reports=DB::table($basedatos.'.order_notes')
                        // ->select('order_notes.id','order_notes.placa','order_notes.servicio','order_notes.date_of_issue','order_notes.conductorname','order_notes.placa','order_notes.total',DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=4 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as gv'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=5 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as go'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=6 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as hyo'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=7 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as lima'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=8 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as estibaje'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=8 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as estibaje'),DB::raw( ' (select name  from '.$basedatos.'.persons where id='.$basedatos.'.order_notes.customer_id ) as cliente'))
                        // ->join(''.$basedatos.'.expenses',''.$basedatos.'.expenses.order_id','=',''.$basedatos.'.order_notes.id')
                        // ->where(''.$basedatos.'.order_notes.placa','=',$request->warehouse_id)
                        // ->groupBy('order_notes.id')
                        // ->orderBy(''.$basedatos.'.order_notes.created_at')->paginate(config('tenant.items_per_page'));
            
                    }
                    else{
                        $reports = DB::table($basedatos.'.expenses')
                        ->select('expenses.id','expenses.placa','expenses.date_of_issue','expenses.total',DB::raw( ' (select name  from '.$basedatos.'.persons where  id='.$basedatos.'.expenses.supplier_id ) as proveedor'),DB::raw( ' (select description  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id  limit 1 ) as descripcionitem'),DB::raw( ' (select sum(galones)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as galones'),DB::raw( ' (select sum(precio)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as precio'),DB::raw( ' (select description from '.$basedatos.'.expense_reasons where  id='.$basedatos.'.expenses.expense_reason_id ) as motivo'))
                    
                        ->whereIn(''.$basedatos.'.expenses.expense_reason_id',['1','2'.'3','4','5','8'])
                        ->where(''.$basedatos.'.expenses.state_type_id','!=','11')
                        ->whereBetween(''.$basedatos.'.expenses.date_of_issue',[$request->dataFabricatiei,$request->dataFabricatiei2])
                        ->orderBy(''.$basedatos.'.expenses.created_at')->paginate(config('tenant.items_per_page'));
                    }
             }
        else
        {
            if($request->warehouse_id && $request->warehouse_id != 'all')
            {
                $reports = DB::table($basedatos.'.expenses')
                ->select('expenses.id','expenses.placa','expenses.date_of_issue','expenses.total',DB::raw( ' (select name  from '.$basedatos.'.persons where  id='.$basedatos.'.expenses.supplier_id ) as proveedor'),DB::raw( ' (select description  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id  limit 1 ) as descripcionitem'),DB::raw( ' (select sum(galones)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as galones'),DB::raw( ' (select sum(precio)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as precio'),DB::raw( ' (select description from '.$basedatos.'.expense_reasons where  id='.$basedatos.'.expenses.expense_reason_id ) as motivo'))
                ->where(''.$basedatos.'.expenses.placa','=',''.$request->warehouse_id.'')
                ->where(''.$basedatos.'.expenses.state_type_id','!=','11')
                ->whereBetween(''.$basedatos.'.expenses.date_of_issue',[$request->dataFabricatiei,$request->dataFabricatiei2])
                 ->orderBy(''.$basedatos.'.expenses.created_at')->paginate(config('tenant.items_per_page'));
              
    
                // $reports=DB::table($basedatos.'.order_notes')
                // ->select('order_notes.id','order_notes.placa','order_notes.servicio','order_notes.date_of_issue','order_notes.conductorname','order_notes.placa','order_notes.total',DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=4 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as gv'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=5 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as go'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=6 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as hyo'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=7 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as lima'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=8 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as estibaje'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=8 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as estibaje'),DB::raw( ' (select name  from '.$basedatos.'.persons where id='.$basedatos.'.order_notes.customer_id ) as cliente'))
                // ->join(''.$basedatos.'.expenses',''.$basedatos.'.expenses.order_id','=',''.$basedatos.'.order_notes.id')
                // ->where(''.$basedatos.'.order_notes.placa','=',$request->warehouse_id)
                // ->groupBy('order_notes.id')
                // ->orderBy(''.$basedatos.'.order_notes.created_at')->paginate(config('tenant.items_per_page'));
    
            }
            else{
                $reports = DB::table($basedatos.'.expenses')
                ->select('expenses.id','expenses.placa','expenses.date_of_issue','expenses.total',DB::raw( ' (select name  from '.$basedatos.'.persons where  id='.$basedatos.'.expenses.supplier_id ) as proveedor'),DB::raw( ' (select description  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id  limit 1 ) as descripcionitem'),DB::raw( ' (select sum(galones)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as galones'),DB::raw( ' (select sum(precio)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as precio'),DB::raw( ' (select description from '.$basedatos.'.expense_reasons where  id='.$basedatos.'.expenses.expense_reason_id ) as motivo'))
                ->where(''.$basedatos.'.expenses.state_type_id','!=','11')
                 ->orderBy(''.$basedatos.'.expenses.created_at')->paginate(config('tenant.items_per_page'));
            }

        }   
      

        $company = Company::first();
        $vehiculos = Vehiculo::select('id', 'placa','marca')->get();
        $warehouses = Warehouse::select('id', 'description')->get();

        return view('inventory::reports.expense.index', compact('reports', 'warehouses','vehiculos','company'));
    }

    /**
     * Search
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request) {

        $reports = Expense::select('id', 'placa','date_of_issue','supplier','total')
            ->where('placa','=',$request->warehouse_id)
            ->latest()->paginate(config('tenant.items_per_page'));

        return view('inventory::reports.expense.index', compact('reports'));
    }

    /**
     * PDF
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function pdf(Request $request) {

        
        $company = Company::first();
        $establishment = Establishment::first();
        ini_set('max_execution_time', 0);

        if($request->ruc=='20604353433'){
            
            $basedatos='tenancy_jl';
        }
        else {
            $basedatos='tenancy_hector';
        }
        

        if($request->tipo=='2')
        {
               
            if($request->warehouse_id && $request->warehouse_id != 'all')
            {
                $reports = DB::table($basedatos.'.expenses')
                ->select('expenses.id','expenses.placa','expenses.date_of_issue','expenses.total',DB::raw( ' (select name  from '.$basedatos.'.persons where  id='.$basedatos.'.expenses.supplier_id ) as proveedor'),DB::raw( ' (select description  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id  limit 1 ) as descripcionitem'),DB::raw( ' (select sum(galones)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as galones'),DB::raw( ' (select sum(precio)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as precio'),DB::raw( ' (select description from '.$basedatos.'.expense_reasons where  id='.$basedatos.'.expenses.expense_reason_id ) as motivo'))
                ->where(''.$basedatos.'.expenses.placa','=',''.$request->warehouse_id.'')
                ->where(''.$basedatos.'.expenses.state_type_id','!=','11')
                ->whereIn(''.$basedatos.'.expenses.expense_reason_id',['6','7'])
                ->whereBetween(''.$basedatos.'.expenses.date_of_issue',[$request->dataFabricatiei,$request->dataFabricatiei2])
                ->latest(''.$basedatos.'.expenses.created_at')->get();
              
    
                // $reports=DB::table($basedatos.'.order_notes')
                // ->select('order_notes.id','order_notes.placa','order_notes.servicio','order_notes.date_of_issue','order_notes.conductorname','order_notes.placa','order_notes.total',DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=4 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as gv'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=5 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as go'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=6 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as hyo'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=7 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as lima'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=8 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as estibaje'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=8 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as estibaje'),DB::raw( ' (select name  from '.$basedatos.'.persons where id='.$basedatos.'.order_notes.customer_id ) as cliente'))
                // ->join(''.$basedatos.'.expenses',''.$basedatos.'.expenses.order_id','=',''.$basedatos.'.order_notes.id')
                // ->where(''.$basedatos.'.order_notes.placa','=',$request->warehouse_id)
                // ->groupBy('order_notes.id')
                // ->orderBy(''.$basedatos.'.order_notes.created_at')->paginate(config('tenant.items_per_page'));
    
            }
            else{
                $reports = DB::table($basedatos.'.expenses')
                ->select('expenses.id','expenses.placa','expenses.date_of_issue','expenses.total',DB::raw( ' (select name  from '.$basedatos.'.persons where  id='.$basedatos.'.expenses.supplier_id ) as proveedor'),DB::raw( ' (select description  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id  limit 1 ) as descripcionitem'),DB::raw( ' (select sum(galones)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as galones'),DB::raw( ' (select sum(precio)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as precio'),DB::raw( ' (select description from '.$basedatos.'.expense_reasons where  id='.$basedatos.'.expenses.expense_reason_id ) as motivo'))
                ->where(''.$basedatos.'.expenses.state_type_id','!=','11')
                ->whereIn(''.$basedatos.'.expenses.expense_reason_id',['6','7'])
                ->whereBetween(''.$basedatos.'.expenses.date_of_issue',[$request->dataFabricatiei,$request->dataFabricatiei2])
                ->latest(''.$basedatos.'.expenses.created_at')->get();
            }
        }
        else if ( $request->tipo=='3')
             {

                    if($request->warehouse_id && $request->warehouse_id != 'all')
                    {
                        $reports = DB::table($basedatos.'.expenses')
                        ->select('expenses.id','expenses.placa','expenses.date_of_issue','expenses.total',DB::raw( ' (select name  from '.$basedatos.'.persons where  id='.$basedatos.'.expenses.supplier_id ) as proveedor'),DB::raw( ' (select description  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id  limit 1 ) as descripcionitem'),DB::raw( ' (select sum(galones)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as galones'),DB::raw( ' (select sum(precio)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as precio'),DB::raw( ' (select description from '.$basedatos.'.expense_reasons where  id='.$basedatos.'.expenses.expense_reason_id ) as motivo'))
                        ->where(''.$basedatos.'.expenses.placa','=',''.$request->warehouse_id.'')
                        ->where(''.$basedatos.'.expenses.state_type_id','!=','11')
                        ->whereIn(''.$basedatos.'.expenses.expense_reason_id',['1','2'.'3','4','5','8'])
                        ->whereBetween(''.$basedatos.'.expenses.date_of_issue',[$request->dataFabricatiei,$request->dataFabricatiei2])
                        ->latest(''.$basedatos.'.expenses.created_at')->get();
                    
            
                        // $reports=DB::table($basedatos.'.order_notes')
                        // ->select('order_notes.id','order_notes.placa','order_notes.servicio','order_notes.date_of_issue','order_notes.conductorname','order_notes.placa','order_notes.total',DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=4 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as gv'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=5 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as go'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=6 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as hyo'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=7 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as lima'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=8 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as estibaje'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=8 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as estibaje'),DB::raw( ' (select name  from '.$basedatos.'.persons where id='.$basedatos.'.order_notes.customer_id ) as cliente'))
                        // ->join(''.$basedatos.'.expenses',''.$basedatos.'.expenses.order_id','=',''.$basedatos.'.order_notes.id')
                        // ->where(''.$basedatos.'.order_notes.placa','=',$request->warehouse_id)
                        // ->groupBy('order_notes.id')
                        // ->orderBy(''.$basedatos.'.order_notes.created_at')->paginate(config('tenant.items_per_page'));
            
                    }
                    else{
                        $reports = DB::table($basedatos.'.expenses')
                        ->select('expenses.id','expenses.placa','expenses.date_of_issue','expenses.total',DB::raw( ' (select name  from '.$basedatos.'.persons where  id='.$basedatos.'.expenses.supplier_id ) as proveedor'),DB::raw( ' (select description  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id  limit 1 ) as descripcionitem'),DB::raw( ' (select sum(galones)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as galones'),DB::raw( ' (select sum(precio)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as precio'),DB::raw( ' (select description from '.$basedatos.'.expense_reasons where  id='.$basedatos.'.expenses.expense_reason_id ) as motivo'))
                        ->where(''.$basedatos.'.expenses.state_type_id','!=','11')
                        ->whereIn(''.$basedatos.'.expenses.expense_reason_id',['1','2'.'3','4','5','8'])
                        ->whereBetween(''.$basedatos.'.expenses.date_of_issue',[$request->dataFabricatiei,$request->dataFabricatiei2])
                        ->latest(''.$basedatos.'.expenses.created_at')->get();
                    }
             }
        else
        {
            if($request->warehouse_id && $request->warehouse_id != 'all')
            {
                $reports = DB::table($basedatos.'.expenses')
                ->select('expenses.id','expenses.placa','expenses.date_of_issue','expenses.total',DB::raw( ' (select name  from '.$basedatos.'.persons where  id='.$basedatos.'.expenses.supplier_id ) as proveedor'),DB::raw( ' (select description  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id  limit 1 ) as descripcionitem'),DB::raw( ' (select sum(galones)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as galones'),DB::raw( ' (select sum(precio)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as precio'),DB::raw( ' (select description from '.$basedatos.'.expense_reasons where  id='.$basedatos.'.expenses.expense_reason_id ) as motivo'))
                ->where(''.$basedatos.'.expenses.placa','=',''.$request->warehouse_id.'')
                ->where(''.$basedatos.'.expenses.state_type_id','!=','11')
                ->whereBetween(''.$basedatos.'.expenses.date_of_issue',[$request->dataFabricatiei,$request->dataFabricatiei2])
                ->latest(''.$basedatos.'.expenses.created_at')->get();
              
    
                // $reports=DB::table($basedatos.'.order_notes')
                // ->select('order_notes.id','order_notes.placa','order_notes.servicio','order_notes.date_of_issue','order_notes.conductorname','order_notes.placa','order_notes.total',DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=4 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as gv'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=5 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as go'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=6 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as hyo'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=7 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as lima'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=8 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as estibaje'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=8 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as estibaje'),DB::raw( ' (select name  from '.$basedatos.'.persons where id='.$basedatos.'.order_notes.customer_id ) as cliente'))
                // ->join(''.$basedatos.'.expenses',''.$basedatos.'.expenses.order_id','=',''.$basedatos.'.order_notes.id')
                // ->where(''.$basedatos.'.order_notes.placa','=',$request->warehouse_id)
                // ->groupBy('order_notes.id')
                // ->orderBy(''.$basedatos.'.order_notes.created_at')->paginate(config('tenant.items_per_page'));
    
            }
            else{
                $reports = DB::table($basedatos.'.expenses')
                ->select('expenses.id','expenses.placa','expenses.date_of_issue','expenses.total',DB::raw( ' (select name  from '.$basedatos.'.persons where  id='.$basedatos.'.expenses.supplier_id ) as proveedor'),DB::raw( ' (select description  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id  limit 1 ) as descripcionitem'),DB::raw( ' (select sum(galones)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as galones'),DB::raw( ' (select sum(precio)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as precio'),DB::raw( ' (select description from '.$basedatos.'.expense_reasons where  id='.$basedatos.'.expenses.expense_reason_id ) as motivo'))
                ->where(''.$basedatos.'.expenses.state_type_id','!=','11')
                ->latest(''.$basedatos.'.expenses.created_at')->get();
            }

        }   




        
       




        
       



        $pdf = PDF::loadView('inventory::reports.expense.report_pdf', compact("reports", "company", "establishment"));
        $filename = 'Reporte_Inventario'.date('YmdHis');

        return $pdf->download($filename.'.pdf');
    }

    /**
     * Excel
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function excel(Request $request) {
        $company = Company::first();
        $establishment = Establishment::first();
        if($request->ruc=='20604353433'){
            
            $basedatos='tenancy_jl';
        }
        else {
            $basedatos='tenancy_hector';
        }


        if($request->tipo=='2')
        {
               
            if($request->warehouse_id && $request->warehouse_id != 'all')
            {
                $records = DB::table($basedatos.'.expenses')
                ->select('expenses.id','expenses.placa','expenses.date_of_issue','expenses.total',DB::raw( ' (select name  from '.$basedatos.'.persons where  id='.$basedatos.'.expenses.supplier_id ) as proveedor'),DB::raw( ' (select description  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id  limit 1 ) as descripcionitem'),DB::raw( ' (select sum(galones)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as galones'),DB::raw( ' (select sum(precio)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as precio'),DB::raw( ' (select description from '.$basedatos.'.expense_reasons where  id='.$basedatos.'.expenses.expense_reason_id ) as motivo'))
                ->where(''.$basedatos.'.expenses.placa','=',''.$request->warehouse_id.'')
                ->where(''.$basedatos.'.expenses.state_type_id','!=','11')
                ->whereIn(''.$basedatos.'.expenses.expense_reason_id',['6','7'])
                ->whereBetween(''.$basedatos.'.expenses.date_of_issue',[$request->dataFabricatiei,$request->dataFabricatiei2])
                ->latest(''.$basedatos.'.expenses.created_at')->get();
              
    
                // $reports=DB::table($basedatos.'.order_notes')
                // ->select('order_notes.id','order_notes.placa','order_notes.servicio','order_notes.date_of_issue','order_notes.conductorname','order_notes.placa','order_notes.total',DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=4 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as gv'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=5 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as go'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=6 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as hyo'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=7 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as lima'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=8 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as estibaje'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=8 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as estibaje'),DB::raw( ' (select name  from '.$basedatos.'.persons where id='.$basedatos.'.order_notes.customer_id ) as cliente'))
                // ->join(''.$basedatos.'.expenses',''.$basedatos.'.expenses.order_id','=',''.$basedatos.'.order_notes.id')
                // ->where(''.$basedatos.'.order_notes.placa','=',$request->warehouse_id)
                // ->groupBy('order_notes.id')
                // ->orderBy(''.$basedatos.'.order_notes.created_at')->paginate(config('tenant.items_per_page'));
    
            }
            else{
                $records = DB::table($basedatos.'.expenses')
                ->select('expenses.id','expenses.placa','expenses.date_of_issue','expenses.total',DB::raw( ' (select name  from '.$basedatos.'.persons where  id='.$basedatos.'.expenses.supplier_id ) as proveedor'),DB::raw( ' (select description  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id  limit 1 ) as descripcionitem'),DB::raw( ' (select sum(galones)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as galones'),DB::raw( ' (select sum(precio)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as precio'),DB::raw( ' (select description from '.$basedatos.'.expense_reasons where  id='.$basedatos.'.expenses.expense_reason_id ) as motivo'))
               
                ->whereIn(''.$basedatos.'.expenses.expense_reason_id',['6','7'])
                ->whereBetween(''.$basedatos.'.expenses.date_of_issue',[$request->dataFabricatiei,$request->dataFabricatiei2])
                ->latest(''.$basedatos.'.expenses.created_at')->get();
            }
        }
        else if ( $request->tipo=='3')
             {

                    if($request->warehouse_id && $request->warehouse_id != 'all')
                    {
                        $records = DB::table($basedatos.'.expenses')
                        ->select('expenses.id','expenses.placa','expenses.date_of_issue','expenses.total',DB::raw( ' (select name  from '.$basedatos.'.persons where  id='.$basedatos.'.expenses.supplier_id ) as proveedor'),DB::raw( ' (select description  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id  limit 1 ) as descripcionitem'),DB::raw( ' (select sum(galones)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as galones'),DB::raw( ' (select sum(precio)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as precio'),DB::raw( ' (select description from '.$basedatos.'.expense_reasons where  id='.$basedatos.'.expenses.expense_reason_id ) as motivo'))
                        ->where(''.$basedatos.'.expenses.placa','=',''.$request->warehouse_id.'')
                        ->where(''.$basedatos.'.expenses.state_type_id','!=','11')
                        ->whereIn(''.$basedatos.'.expenses.expense_reason_id',['1','2'.'3','4','5','8'])
                        ->whereBetween(''.$basedatos.'.expenses.date_of_issue',[$request->dataFabricatiei,$request->dataFabricatiei2])
                        ->latest(''.$basedatos.'.expenses.created_at')->get();
                    
            
                        // $reports=DB::table($basedatos.'.order_notes')
                        // ->select('order_notes.id','order_notes.placa','order_notes.servicio','order_notes.date_of_issue','order_notes.conductorname','order_notes.placa','order_notes.total',DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=4 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as gv'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=5 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as go'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=6 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as hyo'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=7 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as lima'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=8 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as estibaje'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=8 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as estibaje'),DB::raw( ' (select name  from '.$basedatos.'.persons where id='.$basedatos.'.order_notes.customer_id ) as cliente'))
                        // ->join(''.$basedatos.'.expenses',''.$basedatos.'.expenses.order_id','=',''.$basedatos.'.order_notes.id')
                        // ->where(''.$basedatos.'.order_notes.placa','=',$request->warehouse_id)
                        // ->groupBy('order_notes.id')
                        // ->orderBy(''.$basedatos.'.order_notes.created_at')->paginate(config('tenant.items_per_page'));
            
                    }
                    else{
                        $records = DB::table($basedatos.'.expenses')
                        ->select('expenses.id','expenses.placa','expenses.date_of_issue','expenses.total',DB::raw( ' (select name  from '.$basedatos.'.persons where  id='.$basedatos.'.expenses.supplier_id ) as proveedor'),DB::raw( ' (select description  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id  limit 1 ) as descripcionitem'),DB::raw( ' (select sum(galones)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as galones'),DB::raw( ' (select sum(precio)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as precio'),DB::raw( ' (select description from '.$basedatos.'.expense_reasons where  id='.$basedatos.'.expenses.expense_reason_id ) as motivo'))
                        ->where(''.$basedatos.'.expenses.state_type_id','!=','11')
                        ->whereIn(''.$basedatos.'.expenses.expense_reason_id',['1','2'.'3','4','5','8'])
                        ->whereBetween(''.$basedatos.'.expenses.date_of_issue',[$request->dataFabricatiei,$request->dataFabricatiei2])
                        ->latest(''.$basedatos.'.expenses.created_at')->get();
                    }
             }
        else
        {
            if($request->warehouse_id && $request->warehouse_id != 'all')
            {
                $records = DB::table($basedatos.'.expenses')
                ->select('expenses.id','expenses.placa','expenses.date_of_issue','expenses.total',DB::raw( ' (select name  from '.$basedatos.'.persons where  id='.$basedatos.'.expenses.supplier_id ) as proveedor'),DB::raw( ' (select description  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id  limit 1 ) as descripcionitem'),DB::raw( ' (select sum(galones)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as galones'),DB::raw( ' (select sum(precio)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as precio'),DB::raw( ' (select description from '.$basedatos.'.expense_reasons where  id='.$basedatos.'.expenses.expense_reason_id ) as motivo'))
                ->where(''.$basedatos.'.expenses.placa','=',''.$request->warehouse_id.'')
                ->where(''.$basedatos.'.expenses.state_type_id','!=','11')
                ->whereBetween(''.$basedatos.'.expenses.date_of_issue',[$request->dataFabricatiei,$request->dataFabricatiei2])
                ->latest(''.$basedatos.'.expenses.created_at')->get();
              
    
                // $reports=DB::table($basedatos.'.order_notes')
                // ->select('order_notes.id','order_notes.placa','order_notes.servicio','order_notes.date_of_issue','order_notes.conductorname','order_notes.placa','order_notes.total',DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=4 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as gv'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=5 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as go'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=6 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as hyo'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=7 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as lima'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=8 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as estibaje'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=8 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as estibaje'),DB::raw( ' (select name  from '.$basedatos.'.persons where id='.$basedatos.'.order_notes.customer_id ) as cliente'))
                // ->join(''.$basedatos.'.expenses',''.$basedatos.'.expenses.order_id','=',''.$basedatos.'.order_notes.id')
                // ->where(''.$basedatos.'.order_notes.placa','=',$request->warehouse_id)
                // ->groupBy('order_notes.id')
                // ->orderBy(''.$basedatos.'.order_notes.created_at')->paginate(config('tenant.items_per_page'));
    
            }
            else{
                $records = DB::table($basedatos.'.expenses')
                ->select('expenses.id','expenses.placa','expenses.date_of_issue','expenses.total',DB::raw( ' (select name  from '.$basedatos.'.persons where  id='.$basedatos.'.expenses.supplier_id ) as proveedor'),DB::raw( ' (select description  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id  limit 1 ) as descripcionitem'),DB::raw( ' (select sum(galones)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as galones'),DB::raw( ' (select sum(precio)  from '.$basedatos.'.expense_items where  expense_id='.$basedatos.'.expenses.id ) as precio'),DB::raw( ' (select description from '.$basedatos.'.expense_reasons where  id='.$basedatos.'.expenses.expense_reason_id ) as motivo'))
                ->where(''.$basedatos.'.expenses.state_type_id','!=','11')
                ->latest(''.$basedatos.'.expenses.created_at')->get();
            }

        }   



        return (new ExpenseExport)
            ->records($records)
            ->company($company)
            ->establishment($establishment)
            ->download('Reportegasto'.Carbon::now().'.xlsx');
    }
}
