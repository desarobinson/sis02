<?php

namespace Modules\Inventory\Http\Controllers;

use App\Http\Controllers\Controller;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Tenant\Establishment;
use App\Models\Tenant\Company;
use App\Models\Tenant\Item;
use Modules\Inventory\Models\ItemWarehouse;
use Modules\Inventory\Exports\ExpenseExport;
use Modules\Inventory\Models\Warehouse;
use Modules\Expense\Models\Expense;
use Modules\Order\Models\Vehiculo;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class ReportGeneralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        if($request->ruc='20604353433'){
            
            $basedatos='tenancy_jl';
        }
        else {
            $basedatos='tenancy_hector';
        }
        
        
        if($request->warehouse_id && $request->warehouse_id != 'all')
        {

            $reports=DB::table($basedatos.'.order_notes')
            ->select('order_notes.id','order_notes.placa','order_notes.servicio','order_notes.date_of_issue','order_notes.conductorname','order_notes.placa','order_notes.total',DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=4 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as gv'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=5 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as go'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=6 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as hyo'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=7 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as lima'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=8 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as estibaje'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=8 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as estibaje'),DB::raw( ' (select name  from '.$basedatos.'.persons where id='.$basedatos.'.order_notes.customer_id ) as cliente'))
            ->join(''.$basedatos.'.expenses',''.$basedatos.'.expenses.order_id','=',''.$basedatos.'.order_notes.id')
            ->where(''.$basedatos.'.order_notes.placa','=',$request->warehouse_id)
            ->groupBy('order_notes.id')
            ->orderBy(''.$basedatos.'.order_notes.created_at')->paginate(config('tenant.items_per_page'));
           
           
        }
        else{
            
            $reports=DB::table($basedatos.'.order_notes')
            ->select('order_notes.id','order_notes.placa','order_notes.servicio','order_notes.date_of_issue','order_notes.conductorname','order_notes.placa','order_notes.total',DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=4 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as gv'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=5 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as go'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=6 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as hyo'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=7 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as lima'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=8 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as estibaje'),DB::raw( ' (select sum('.$basedatos.'.expenses.total) as totalgasto  from '.$basedatos.'.expenses where expense_reason_id=8 and order_id='.$basedatos.'.order_notes.id and state_type_id !=11) as estibaje'),DB::raw( ' (select name  from '.$basedatos.'.persons where id='.$basedatos.'.order_notes.customer_id ) as cliente'))
            ->join(''.$basedatos.'.expenses',''.$basedatos.'.expenses.order_id','=',''.$basedatos.'.order_notes.id')
            
            ->groupBy('order_notes.id')
            ->orderBy(''.$basedatos.'.order_notes.created_at')->paginate(config('tenant.items_per_page'));
        }

        $company = Company::first();
        $vehiculos = Vehiculo::select('id', 'placa','marca')->get();
        $warehouses = Warehouse::select('id', 'description')->get();

        return view('inventory::reports.general.index', compact('reports', 'warehouses','vehiculos','company'));
    
        // $reports=  DB::table($request->estable_id.'.items')
        // ->select('items.id','items.description','stock',DB::raw('sum('.$request->estable_id.'.inventario_items.quantity) as quantity'),'sale_unit_price','purchase_unit_price',''.$request->estable_id.'.warehouses.description as almacen' )
        // ->leftjoin(''.$request->estable_id.'.inventario_items',''.$request->estable_id.'.inventario_items.item_id','=','items.id')
        // ->join(''.$request->estable_id.'.inventarios',''.$request->estable_id.'.inventarios.id','=',''.$request->estable_id.'.inventario_items.inventario_id')
        // ->join(''.$request->estable_id.'.warehouses',''.$request->estable_id.'.warehouses.id','=',''.$request->estable_id.'.inventario_items.warehouse_id')
        
        // ->where(''.$request->estable_id.'.inventario_items.warehouse_id', $request->warehouse_id)
        // ->groupBy('items.id','items.description')
        // ->orderBy(''.$request->estable_id.'.inventarios.created_at')->paginate(config('tenant.items_per_page'));
            
    }

    /**
     * Search
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request) {

        $reports = Expense::select('id', 'placa','date_of_issue','supplier','total')
            ->where('placa','=',$request->warehouse_id)
            ->latest()->paginate(config('tenant.items_per_page'));

        return view('inventory::reports.general.index', compact('reports'));
    }

    /**
     * PDF
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function pdf(Request $request) {

        $company = Company::first();
        $establishment = Establishment::first();
        ini_set('max_execution_time', 0);

        if($request->warehouse_id && $request->warehouse_id != 'all')
        {
            $reports = Expense::select('id', 'placa','date_of_issue','supplier','total')
            ->where('placa','=',$request->warehouse_id)
            ->latest()->get();
        }
        else{

            $reports = Expense::select('id', 'placa','date_of_issue','supplier','total')
            ->latest()->get();
        }



        $pdf = PDF::loadView('inventory::reports.expense.report_pdf', compact("reports", "company", "establishment"));
        $filename = 'Reporte_Inventario'.date('YmdHis');

        return $pdf->download($filename.'.pdf');
    }

    /**
     * Excel
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function excel(Request $request) {
        $company = Company::first();
        $establishment = Establishment::first();


        if($request->warehouse_id && $request->warehouse_id != 'all')
        {
            $records = Expense::select('id', 'placa','date_of_issue','supplier','total')
            ->where('placa','=',$request->warehouse_id)
            ->latest()->get();

        }
        else{
            $records = Expense::select('id', 'placa','date_of_issue','supplier','total')
            ->latest()->get();

        }


        return (new ExpenseExport)
            ->records($records)
            ->company($company)
            ->establishment($establishment)
            ->download('Reportegasto'.Carbon::now().'.xlsx');
    }
}
